# Changelog
Todos los cambios notables serán documentados en este fichero.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.4] - 2021-12-11
### Added
- Se añade arquitectura cliente - servidor: desde el cliente se controlan las teclas del juego y el servidor dibuja los movimientos.

## [1.3] - 2021-11-19
### Added
- Se añade logger que muestra por pantalla las anotaciones realizadas por los jugadores.
- Se añade bot que controla la raqueta del jugador 1.
- Se añade nueva funcionalidad: el juego termina cuando alguno de los jugadores alcance los 3 puntos.

## [1.2] - 2021-10-26
### Added
- Fichero README.md con las instrucciones del juego.
- Se añade movimiento a las raquetas y a la pelota.
- Se añade nueva funcionalidad: el tamaño de la pelota se reduce según avanza el juego.

## [1.1] - 2021-10-19
### Added
- Fichero CHANGELOG.md para documentar cambios en el repositorio.
- Se indica la autoría del código en cada fichero.

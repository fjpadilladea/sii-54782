# Tenis

## Instrucciones

### Jugador 1:
- Subir raqueta: W
- Bajar raqueta: S
### Jugador 2:
- Subir raqueta: O
- Bajar raqueta: L

## Funcionalidades
- Servidor juego tenis.
- Cliente juego tenis.
- Programa logger.
- Programa bot.

### Para usar todas las funcionalidades:
Ejectuar primero el logger, luego el cliente tenis, luego el servidor tenis y por último el bot.

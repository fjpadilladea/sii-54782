#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <time.h>

#include "DatosMemCompartida.h"

int main(void){
	DatosMemCompartida *pdatos;
	int fd;
	clock_t t;
	
	fd = open("/tmp/fichero_bot",O_RDWR);
	pdatos = (DatosMemCompartida *) mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
	close(fd);
	
	while(1){
		usleep(25000);
		
		//BOT 1
		//Si la esfera está por encima del límite superior de la raqueta
		if(pdatos->esfera.centro.y > pdatos->raqueta1.y1){
			pdatos->accion = 1;
		}
	
		//Si la esfera está por debajo del límite inferior de la raqueta
		else if(pdatos->esfera.centro.y < pdatos->raqueta1.y2){
			pdatos->accion = -1;
		}
		else {
			pdatos->accion = 0;
		}
		
	}
	
}

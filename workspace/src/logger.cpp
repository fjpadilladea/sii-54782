#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>

// Estructura para enviar la información a través del FIFO
typedef struct informacion{
	int jugador; // identifica jugador 1 o 2
	int puntuacion;
} informacion;

int main(int argc, char **argv) {
	int fd;
	
	// borra FIFO por si existía previamente
        unlink("/tmp/FIFO_logger");
   	/* crea el FIFO */
    	if (mkfifo("/tmp/FIFO_logger", 0600)<0) {
        	std::cerr<<"No puede crearse el FIFO logger";
        	return(1);
    	}
    	/* Abre el FIFO */
    	if ((fd=open("/tmp/FIFO_logger", O_RDONLY))<0) {
        	std::cerr<<"No puede abrirse el FIFO logger";
        	return(1);
    	}
    	
       informacion info;
       while (read(fd, &info, sizeof(info))==sizeof(info)) {
       	if(info.jugador == 1){
       		std::cout << "Jugador 1 marca un punto, lleva un total de " 					  << info.puntuacion <<" puntos." << std::endl;
       	}
       	else if(info.jugador == 2){
       		std::cout << "Jugador 2 marca un punto, lleva un total de " 					  << info.puntuacion <<" puntos." << std::endl;
       	}
    	}
    	close(fd);
    	unlink("/tmp/FIFO_logger");
}
